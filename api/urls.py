from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.urls import path,include
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.views import obtain_auth_token

from . import views

app_name = 'api'


urlpatterns = [
    # Shops
    url('shops/', views.ShopListAPIView.as_view(), name="shopslist"),
    url(r'^newshop/$', views.ShopCreateAPIView.as_view(), name="new_shop"),
    url('detail/(?P<shop_id>[0-9]+)/', views.ShopDetailAPIView.as_view(), name="shop_detail"),
    url('edit/(?P<pk>[0-9]+)/', views.ShopRetrieveAPIView.as_view(), name="shop_retrieve"),
    
    # Contact
    url('contact/', views.ContactAPIView.as_view(), name="contact"),

    # Reviews
    url('reviews/', views.ReviewListAPIView.as_view(), name="reviews"),
    url('newreview', views.ReviewCreateAPIView.as_view(), name="new_review"),
    url('deletereview/(?P<rev_id>[0-9]+)', views.ReviewDeleteAPIView.as_view(), name="delete_review"),
    
    # Category
    url('category/', views.CategoryListAPIView.as_view(), name="category"),
    path('cat/<str:cat>/', views.CategoryAPIView.as_view(), name="category"),

    # Account
    url('accounts/', views.AccountListAPIView.as_view(), name="accounts"),
    url('register/', views.RegisterAPIView.as_view(), name="new_account"),
    url('login/', views.UserLoginAPIView.as_view(), name="login"),
    url('logout/', views.UserLogoutAPIView.as_view(), name="logout"),
    path('token/', views.tokenAPI.as_view(), name='token'),
    path('profile/<str:username>/', views.AccountProfileAPIView.as_view(), name="profile"),
    path('modifyaccount/<str:username>/', views.ModifyAccountAPIView.as_view(), name="modify_account"),
]

from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework.serializers import SerializerMethodField, ValidationError
from django.db.models import Q


#Import ACCOUNT models
from account.models import User, Account

# Import MAINSHOP models
from mainshop.models import Shop, Review, Category

categories = Category.objects.all().values_list('name', 'name')
categories_list = []

for item in categories:
    categories_list.append(item)

# Serializer ACCOUNT

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField()
    class Meta:
        model = Account
        fields = ['username','email', 'first_name', 'last_name', 'password']

        extra_kwargs = {"password": {"write_only": True} }
    
    def validate(self, data):
        data = self.get_initial()
        username = data.get("username")
        email = data.get("email")
        user_name = User.objects.filter(username=username)
        user_email = User.objects.filter(email = email)
        if user_name.exists() or user_email.exists():
            raise ValidationError("This username or email is already used.")
        
        return data


    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        password = self.validated_data['password']
        
        user_obj = User(
                username = username,
                email = email,
                first_name=first_name,  
                last_name=last_name
            )
        user_obj.set_password(password)
        user_obj.save()

        account_obj = Account(user=user_obj, username= username, first_name=first_name, last_name=last_name, email=email)
        account_obj.save()
        return validated_data

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'

class AccountCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField()
    class Meta:
        model = Account
        fields = ['id','username','email', 'first_name', 'last_name', 'password']

class AccountModifySerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['username','email', 'first_name', 'last_name']

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

# Serializer CONTACT

class ContactSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.CharField()
    message = serializers.CharField()


# Serializer MAINSHOP

class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ['id','name', 'website', 'categ', 'averageRating', 'logo']
        
        widgets={
            'categ': serializers.ChoiceField(choices=categories_list),
        }

    
class ShopCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ['name', 'website', 'categ', 'logo']

        widgets={
            'categ': serializers.ChoiceField(choices=categories_list),
        }


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class ReviewSerializer(serializers.ModelSerializer):
    user = SerializerMethodField()
    shop = SerializerMethodField()
    class Meta:
        model = Review
        fields = ['id','title', 'comment', 'rating', 'user', 'shop','rev_date', 'likes']

    def get_user(self,obj):
        return str(obj.user.username)

    def get_shop(self,obj):
        return str(obj.shop.name)

class ReviewNewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ['user','shop','title', 'comment', 'rating']

        def validate(self,data):
            shop = data['shop']
            if not Shop.objects.filter(name=shop):
                raise serializers.ValidationError(('Shop not found'))

            
            rating = data['rating']
            if int(rating) <= 0 and int(rating) > 5:
                raise serializers.ValidationError(('Wrong value for rating'))

            return data

class starSerializer(serializers.ModelSerializer):
    class Meta:
            model = Review
            fields = ['rating']

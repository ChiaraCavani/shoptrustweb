import django
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail
from knox.models import AuthToken
from django.db.models import Avg

#REST IMPORTS
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, DestroyAPIView, ListCreateAPIView
#Import MODELS
from shopreviews import settings
from mainshop.models import Shop, Review, Category
from account.models import User, Account


#Import SERIALIZER
from api.serializer import *

# Create your views here.


"""
MAINSHOP

"""

# SHOPS
class ShopCreateAPIView(CreateAPIView):
    queryset=Shop.objects.all()
    serializer_class = ShopCreateSerializer
    permission_classes = [IsAdminUser]


class ShopDetailAPIView(RetrieveAPIView):
    permission_classes = [AllowAny]
    def get(self,request,shop_id):
        shop = Shop.objects.get(id=shop_id)
        reviews = Review.objects.filter(shop=shop_id).order_by("-date_pub")
        average = reviews.aggregate(Avg("rating"))["rating__avg"]
            
        if average == None:
            average = 0
        average = round(average, 2)
        shop.averageRating=average
        shop.save()
        context = { "shop": ShopSerializer(shop).data, 'review': ReviewSerializer(reviews, many=True).data}  
    
        return Response(context, status=status.HTTP_200_OK)
  
  
class ShopRetrieveAPIView(RetrieveUpdateDestroyAPIView):
    queryset=Shop.objects.all()
    serializer_class = ShopSerializer
    permission_classes = [IsAdminUser]


class ShopListAPIView(ListAPIView):
    serializer_class = ShopSerializer
    permission_classes = [AllowAny]
    
    def get_queryset(self, *args, **kwargs):
        queryset_list=Shop.objects.all()
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(Q(name__icontains=query))
        return queryset_list

#CONTACT

class ContactAPIView(CreateAPIView):
    serializer_class = ContactSerializer
    permission_classes = [AllowAny]

    def post(self, request):
        serializer=ContactSerializer(data=request.data)
        
        if serializer.is_valid():
            name = serializer.data['name']
            email = serializer.data['email']
            message = serializer.data['message']

             # Send email
            send_mail(
                'Message from ' + name, # Subject
                message, # Message
                email, # From email
                [settings.EMAIL_HOST_USER], # To email   

            )

            return Response(serializer.data,status=status.HTTP_200_OK)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)



#REVIEW

class ReviewListAPIView(ListAPIView):
    queryset=Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [AllowAny]

class ReviewCreateAPIView(CreateAPIView):
    queryset=Review.objects.all()
    serializer_class = ReviewNewSerializer
    permission_classes = [AllowAny]

    def post(self, request):
        serializer=ReviewNewSerializer(data=request.data)
        
        if serializer.is_valid():
            user = serializer.data['user']
            shop = serializer.data['shop']
            title = serializer.data['title']
            comment = serializer.data['comment']
            rating = serializer.data['rating']

            shop_rev = Shop.objects.get(id=shop)
            reviews = Review.objects.filter(shop=shop).order_by("-date_pub")
            count_rev = reviews.count()
            new_rate = (shop_rev.averageRating + rating) / (count_rev + 1)
            shop_rev.averageRating = new_rate
            shop_rev.save()
            
            # Validate User and Shop
            shop_obj = Shop.objects.get(id=shop)
            user_obj = User.objects.get(id=user)

            # New Review
            review = Review(shop=shop_obj, user=user_obj, title=title, comment=comment, rating=rating)
            review.save()

            return Response(serializer.data,status=status.HTTP_200_OK)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

class ReviewDeleteAPIView(DestroyAPIView):
    queryset=Review.objects.all()
    serializer_class = ReviewNewSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, rev_id):
        review = Review.objects.get(id=rev_id)
        rate_review = review.rating
        shop = Shop.objects.get(name=review.shop)
        rate_shop= shop.averageRating
        reviews_shop = Review.objects.filter(shop=shop).count()

        # Compute the rating after delete a review
        new_rate = ((rate_shop)*(reviews_shop))-(rate_review)
        print(new_rate)

        if review.user != self.request.user:
            return Response(data={'Error': 'You can not delete this review because you are not the author'}, status=status.HTTP_400_BAD_REQUEST)
        
        if review.user == self.request.user:
            shop.averageRating = new_rate
            shop.save()
            self.perform_destroy(review)
            return Response("OK",status=status.HTTP_204_NO_CONTENT)



# CATEGORY
class CategoryListAPIView(ListCreateAPIView):
    queryset=Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

class CategoryAPIView(APIView):
    permission_classes = [AllowAny]
    def get(self,request,cat):
        shops=Shop.objects.filter(categ=cat).order_by('name')
        
        context = {"category": cat, "shop": ShopSerializer(shops, many=True).data}
   
        return Response(context, status=status.HTTP_200_OK)


"""
ACCOUNT

"""

# List accounts
class AccountListAPIView(ListAPIView):
    queryset=Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

# Registration User and Account 
class RegisterAPIView(CreateAPIView):
    queryset=Account.objects.all()
    serializer_class = RegisterSerializer
    permission_classes = [AllowAny]

        
    def post(self,request):
        
        serializer = RegisterSerializer(data=request.data)
        if serializer.is_valid():
            username = serializer.data['username']
            email = serializer.data['email']
            password = serializer.data['password']
            first_name = serializer.data['first_name']
            last_name = serializer.data['last_name']
          
            if User.objects.filter(Q(username = username) | Q(email = email)):
                error = 'Username or email already in use.'
                return Response( {'error':error}, status=status.HTTP_409_CONFLICT)

            # New user
            user = User.objects.create(username=username, email=email, first_name=first_name, last_name=last_name)
            user.set_password(password)
            user.save()  

            # New Account
            account = Account(user=user, username= username, first_name=first_name, last_name=last_name, email=email)
            account.save() 

            login(request, authenticate(username=username, password=password))
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self,request):
        
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            
            username = serializer.data['username']
            password = serializer.data['password']
            print(username, password)
            # authenticate
            user = authenticate(username=username, password=password)
            if user and user.is_active:
                # log the user in!
                login(request, user)
                return Response(serializer.data,status=status.HTTP_200_OK)
            error = 'Wrong username or password.'
            return Response({'error':error},status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
        
class UserLogoutAPIView(APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self,request):
        logout(request)
        return Response(None,status=status.HTTP_200_OK)

class AccountProfileAPIView(RetrieveAPIView):
    permission_classes = [AllowAny]
    def get(self,request,username):
        account = Account.objects.get(username=username)
        reviews = Review.objects.filter(user=account.user.id).order_by("-date_pub")
        count = reviews.count()

        average = reviews.aggregate(Avg("rating"))["rating__avg"]
        if average == None:
            average = 0
        average = round(average, 2)
       
        context = { "account": AccountSerializer(account).data, 'review': ReviewSerializer(reviews, many=True).data, 'average':average}  
    
        return Response(context, status=status.HTTP_200_OK)

class tokenAPI(APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self,request):
        context = {
            'token':django.middleware.csrf.get_token(request)
        }
        return Response(context, status=200)

class ModifyAccountAPIView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = AccountModifySerializer
    
    
    def post(self, request, username):
        serializer=AccountModifySerializer(data=request.data)
        account=Account.objects.get(username=username)
        user=User.objects.get(username=username)
        
        if serializer.is_valid():
            username = serializer.data['username']
            email = serializer.data['email']
            first_name = serializer.data['first_name']
            last_name = serializer.data['last_name']

            account.username = username
            account.email = email
            account.first_name = first_name
            account.last_name = last_name

            user.username=username
            user.email = email
            user.first_name=first_name
            user.last_name = last_name

            account.save()
            user.save()

              
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        








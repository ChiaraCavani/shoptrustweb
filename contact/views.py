from django.shortcuts import render
from .forms import *
from django.core.mail import send_mail
from django.conf import settings

# Create your views here.
def contact(request):
    if request.method=='GET':
        form = ContactForm()
    else:
        form=ContactForm(request.POST)
        if form.is_valid():
            message_email = form.cleaned_data['email']
            message_name = form.cleaned_data['name']
            message = form.cleaned_data['message']
            
            # Send email
            send_mail(
                'Message from ' + message_name, # Subject
                message, # Message
                message_email, # From email
                [settings.EMAIL_HOST_USER], # To email   

            )
            
            message = "Thanks " + message_name + ", your email is send well!"
            return render(request, 'contact/contact.html', {"form": form, "message": message })   

            
    return render(request, 'contact/contact.html', {"form": form})

import pprint

from django.test import TestCase, Client
from django.urls import reverse
from account.models import User, Account
from account.forms import RegisterForm, AccountForm, LoginForm

class TestAccountForms(TestCase):

    def test_login_form(self):
        form = LoginForm(data={
            'username':'Chiara',
            'password':'Maddy17'
        })

        self.assertTrue(form.is_valid())

    def test_login_form_username_required(self):
        form = LoginForm(data={
            'password':'Maddy17'
        })

        self.assertFalse(form.is_valid())

    def test_login_form_password_required(self):
        form = LoginForm(data={
            'username':'Chiara'
        })

        self.assertFalse(form.is_valid())

    def test_account_profile_form(self):
        form = RegisterForm(data={
            'username':'Martina',
            'password':'marti',
            'email':'marti.levizzani@gmail.com',
            'biography':'-',
            'mobile':3384014188,
            'name':'Martina',
            'surname':'Levizzani',
        })

        self.assertTrue(form.is_valid())

    def test_account_profile_form_valid_email(self):
        form = RegisterForm(data={
            'username': 'Chiara',
            'first_name': 'Chiara',
            'last_name': 'Cavani',            
            'email': 'chiara.cavani@outlook.it',
            'password': 'Maddy17',
        })

        self.assertFalse(form.is_valid())
        self.assertTrue(len(form.errors),1)

    def test_registration_form_all_required(self):
        form = RegisterForm(data={
        })

        self.assertFalse(form.is_valid())
        self.assertTrue(len(form.errors),7)

    




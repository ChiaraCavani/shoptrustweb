from django.test import SimpleTestCase
from django.urls import reverse,resolve
from account.views import *


class TestAccountUrls(SimpleTestCase):

    def test_account_login_url(self):
        self.assertEquals(resolve(reverse('account:login')).func, login_user)
    def test_registration_url(self):
        self.assertEquals(resolve(reverse('account:registration')).func, registration)
    def test_account_logout_url(self):
        self.assertEquals(resolve(reverse('account:logout')).func, logout_user)
    
    def test_account_profile_url(self):
        self.assertEquals(resolve(reverse('account:profile')).func, profile)
    def test_account_fav_url(self):
        self.assertEquals(resolve(reverse('account:fav')).func, fav)
    def test_account_modify_url(self):
        self.assertEquals(resolve(reverse('account:modify')).func, update_account)

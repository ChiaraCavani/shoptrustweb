import pprint

from django.test import TestCase, Client
from django.urls import reverse
from account.models import User,Account

class TestAccountViews(TestCase):

    def checkAccount(self):
        self.client = Client()
        user = User.objects.create(username='Chiara')
        user.set_password('Maddy17')
        user.save()
        self.redirect_url = '/?next='
        self.login_url = reverse("account:login")
        self.registration_url = reverse('account:registration')
        self.logout_url = reverse('account:logout')
        self.profile_url = reverse('account:profile')
        self.fav_url = reverse('account:fav')
        self.modify_url = reverse('account:modify')

    def test_login(self):
        get_response = self.client.get(self.login_url)
        post_response = self.client.post(self.login_url)

        self.assertEquals(get_response.status_code, 200)
        self.assertEquals(post_response.status_code, 302)

     def test_registration(self):
        get_response = self.client.get(self.registration_url)
        post_response = self.client.post(self.registration_url)

        self.assertEquals(get_response.status_code, 200)
        self.assertEquals(post_response.status_code, 302)

    def test_logout(self):
        get_response = self.client.get(self.logout_url)
        post_response = self.client.post(self.logout_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302)

    def test_profile(self):
        get_response = self.client.get(self.profile_url)
        post_response = self.client.post(self.profile_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_fav(self):
        get_response = self.client.get(self.fav_url)
        post_response = self.client.post(self.fav_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_modify(self):
        get_response = self.client.get(self.modify_url)
        post_response = self.client.post(self.modify_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 


    def test_login_post(self):
        post_response = self.client.post(self.login_url,{
            'username':'Chiara',
            'password':'Maddy17'
        })
        self.assertEquals(post_response.status_code, 302)
        self.assertEquals(post_response.url, reverse('mainshop:home'))

    def test_login_wrong_username(self):
        post_response = self.client.post(self.login_url,{
            'username':'Chiara',
            'password':'Maddy17'
        })
        self.assertEquals(post_response.status_code, 302)
        self.assertEquals(post_response.context['error'], 'Wrong username or password.')

    def test_login_wrong_password(self):
        self.client.logout()
        post_response = self.client.post(self.login_url,{
            'username':'Chiara',
            'password':'Maddy17'
        })
        self.assertEquals(post_response.status_code, 302)
        self.assertEquals(post_response.context['error'], 'Wrong username or password.')

   
    def test_registration_post(self):
        data = {
            'username': 'Chiara',
            'first_name': 'Chiara',
            'last_name': 'Cavani',            
            'email': 'chiara.cavani@outlook.it',
            'password': 'Maddy17',
        }

        post_response = self.client.post(self.registration_url, data)

        self.assertEquals(post_response.status_code, 302)
        self.assertEquals(post_response.url, reverse('mainshop:home'))

    
    def test_account_created(self):
        data = {
            'username': 'Chiara',
            'first_name': 'Chiara',
            'last_name': 'Cavani',            
            'email': 'chiara.cavani@outlook.it',
            'password': 'Maddy17',
        }

        post_response = self.client.post(self.registration_url, data)
        self.assertTrue(Account.objects.get(user=User.objects.get(username='Chiara')))

    def test_account_already_in_use(self):
        data = {
            'username': 'Chiara',
            'first_name': 'Chiara',
            'last_name': 'Cavani',            
            'email': 'chiara.cavani@outlook.it',
            'password': 'Maddy17',
        }

        user = User.objects.create(username='Chiara')
        user.save()

        post_response = self.client.post(self.registration_url,data)

        self.assertEquals(post_response.status_code, 302)
        self.assertEquals(post_response.context['error'], 'Username or email already in use.')

    def test_registration_form_not_valid(self):
        data = {
            'username': 'Chiara',
            'first_name': 'Chiara',
            'last_name': 'Cavani',            
            'email': 'chiara.cavani@outlook.it',
            'password': 'Maddy17',
        }

        post_response = self.client.post(self.registration_url,data)

        self.assertEquals(post_response.status_code, 302)
        self.assertEquals(len(post_response.context['form'].errors), 1)

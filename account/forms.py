from django import forms
from django.db import models
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


from .models import *

class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']

    def clean_username(self):
        data = self.cleaned_data
        username = data['username']
        if len(username) > 20:
            raise forms.ValidationError('Max 20 characters')
        return username

    def clean_firstname(self):
        data = self.cleaned_data
        firstname = data['first_name']
        if not re.fullmatch('[a-zA-Z]+', firstname):
            raise forms.ValidationError("Don't use special characters")
        return firstname

    def clean_lastname(self):
        data = self.cleaned_data
        lastname = data['lastname']
        if not re.fullmatch('[a-zA-Z]+', lastname):
            raise forms.ValidationError("Don't use special characters")
        return lastname

    def clean_email(self):
        data = self.cleaned_data
        email = data['email']
        if len(email) > 50:
            raise forms.ValidationError("Invalid email")
        return email

    def clean_image(self):
        data = self.cleaned_data
        image = data['image']
        return image
        
        
class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['username', 'first_name', 'last_name', 'email', 'image']


class LoginForm(forms.Form):
    username = forms.CharField(label='Enter your username')
    password = forms.CharField(label='Enter your password', widget=forms.PasswordInput())


# Generated by Django 3.1 on 2020-08-27 14:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainshop', '0010_remove_shop_favourite'),
        ('account', '0011_auto_20200819_1203'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='favourite',
            field=models.ManyToManyField(blank=True, related_name='favourite', to='mainshop.Shop'),
        ),
    ]

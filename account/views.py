from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.db.models import Q
from http import HTTPStatus
from django.db.models import Avg



from .forms import *
from .models import *
from mainshop.models import Review


# Here you can register a new account 
def registration(request):
    # If the user is already login stay in the home
    if request.user.is_authenticated:
        return redirect("mainshop:home")
    
    else:
        # Registration
        if request.method == "POST":
            form = RegisterForm(request.POST or None)
            # Check if the form is valid
            if form.is_valid():
                raw_password = form.cleaned_data.get('password')
                username = form.cleaned_data.get('username')
                email = form.cleaned_data.get('email')
                first_name = form.cleaned_data.get('first_name')
                last_name = form.cleaned_data.get('last_name')
                
                # Check if the account already exists
                if User.objects.filter(Q(username = username) | Q(email = email)):
                    error = 'Username or email already in use.'
                    return render(request, 'account/registration.html', {'form': form, 'error':error}, status=HTTPStatus.FOUND)
              
                # If the user is new create it
                user = User.objects.create(username=username, email=email, first_name=first_name, last_name=last_name)
                user.set_password(raw_password)
                user.save()                     
               
                # Authenticate the user
                user = authenticate(username=user.username, password=raw_password)

                # Create the relative account
                Account.objects.create(user=user, username= username, first_name=first_name, last_name=last_name, email=email)
            
                # Login the user
                login(request, user)
        
                return redirect("mainshop:home")
        else:
            form = RegisterForm()
        return render(request, "account/registration.html", {"form": form})


# Log-in
def login_user(request):
    # If the user is already login stay in the home
    if request.user.is_authenticated:
        return redirect("mainshop:home")
    
    else: 
        # Login
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                username = request.POST.get('username')
                mypassword = request.POST.get('password')
               
                # Check the credentials
                user = authenticate(username=username, password=mypassword)
               
                if user is not None:
                    # If user exists
                    if user.is_active:
                        login(request, user)
                        return redirect("mainshop:home")
                    else:
                        return render(request, 'account/login.html', {"form": form, "error": "Your account has been disabled."})
                else:
                    return render(request, 'account/login.html', {"form": form, "error": "Invalid Username or Password! Try Again."})
        
            return redirect("mainshop:home")
        else:
            form = LoginForm(request.POST)
        
        return render(request, 'account/login.html', {"form": form} )


#Log-out user
def logout_user(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect("mainshop:home")
    else:
        return redirect("mainshop:home")


def profile(request, id):
    # User profile with his reviews
    user = User.objects.get(id=id)
    account = Account.objects.get(user=user)
    review = Review.objects.filter(user=id).order_by("-date_pub")
    count = review.count()

    # Compute the rating of the User (average of his reviews)
    average = review.aggregate(Avg("rating"))["rating__avg"]
    if average == None:
        average = 0
    average = round(average, 2)

    # Check if reviews exists else display error
    error = None
    if not review.exists():
        error = 'Still no reviews'
        return render(request, 'account/profile.html', {'error':error, 'prof':user, 'account': account, 'count':count,
        'average':average,}, status=HTTPStatus.FOUND)
   
    context = {
        'account': account,
        'prof': user,
        'reviews': review,
        'count':count,
        'average':average,
          
    }

    return render(request,'account/profile.html', context)

@login_required
def update_account(request):
    # Get the username
    username = request.user.account.username
    
    if request.method == 'POST':
        update_form = AccountForm(request.POST,
                                   request.FILES,
                                   instance=request.user.account)
        
        form_username = request.POST.get('username')
        form_email = request.POST.get('email')
                
        # Check if the new username already exists
        if Account.objects.filter(Q(username = form_username)) and username != form_username:
            messages.error(request, 'Username already exist')
            return redirect("account:modify")
        
               
        if update_form.is_valid() :
            update_form.save()
            # Change also user information
            user = request.user
            user.username=form_username
            user.email=form_email
            user.save()
            
            messages.success(request, 'Your account has been updated!')
            return redirect("account:profile", request.user.id)

    else:
        update_form = AccountForm(instance=request.user)

    context = {
        'update_form': update_form,
        
    }

    return render(request, 'account/modify.html', context)


def fav(request, id):
    # Get the favourites shops of the User
    user = User.objects.get(id=id)
    account = Account.objects.get(user=user)
    shops = account.favourite.all()
    
    return render(request, 'account/favorite.html', {'shops':shops})


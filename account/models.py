from django.db import models
from django.contrib.auth.models import User
from mainshop.models import Shop

# Create your models here.

PROFILE_PIC = "static/profile_pics/"
DEFAULT="static/profile_pics/default.jpg"

class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=20, null=True)
    first_name = models.CharField(max_length=20, null=True)
    last_name = models.CharField(max_length=20, null=True)
    email = models.EmailField(max_length=50, null=True)
    image = models.ImageField(upload_to=PROFILE_PIC, default=DEFAULT, null=True, blank=True)
    favourite = models.ManyToManyField(Shop, related_name='favourites', blank=True)

    def __str__(self):
        return f'{self.user.username} Profile'
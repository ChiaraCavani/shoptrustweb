from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = "account"

urlpatterns = [
    path('registration/', views.registration, name="registration"),
    path('', views.login_user, name="login"),
    path('logout/', views.logout_user, name="logout"),
    url('profile/(?P<id>[0-9]+)/', views.profile, name="profile"),
    url('fav/(?P<id>[0-9]+)/', views.fav, name="fav"),
    path('modify/', views.update_account, name="modify")
    
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
from django.test import SimpleTestCase
from django.urls import reverse,resolve
from mainshop.views import *

class TestMainUrls(SimpleTestCase):

    def test_home_url(self):
        self.assertEquals(resolve(reverse('mainshop:home')).func, home)
    def test_detail_shop_url(self):
        self.assertEquals(resolve(reverse('mainshop:detail')).func, detail)
    def test_new_shop_url(self):
        self.assertEquals(resolve(reverse('mainshop:newshop')).func, new_shop)
    def test_change_shop_url(self):
        self.assertEquals(resolve(reverse('mainshop:change_shop')).func, change_shop)
    def test_delete_shop_url(self):
        self.assertEquals(resolve(reverse('mainshop:delete_shop')).func, delete_shop)
    def test_review_url(self):
        self.assertEquals(resolve(reverse('mainshop:review')).func, review)
    def test_delete_review_url(self):
        self.assertEquals(resolve(reverse('mainshop:eliminate')).func, eliminate)
    def test_like_review_url(self):
        self.assertEquals(resolve(reverse('mainshop:likereview')).func, like_rev)
    def test_search_url(self):
        self.assertEquals(resolve(reverse('mainshop:search_results')).func, searchShop)
    def test_category_url(self):
        self.assertEquals(resolve(reverse('mainshop:category')).func, category)
    def test_category_list_url(self):
        self.assertEquals(resolve(reverse('mainshop:category_list')).func, category_list)
    def test_add_category_url(self):
        self.assertEquals(resolve(reverse('mainshop:add_category')).func, add_category)
    def test_delete_category_url(self):
        self.assertEquals(resolve(reverse('mainshop:delete_category')).func, delete_category)
    
    
    
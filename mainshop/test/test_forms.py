import pprint

from django.test import TestCase, Client
from django.urls import reverse
from mainshop.models import Category, Shop, Review
from mainshop.forms import ShopForm, ReviewForm, CategoryForm

class TestMainForms(TestCase):

    def test_shop_form(self):
        form = ShopForm(data={
            'name': 'ASOS',
            'website': 'https://www.asos.com/it',
            'logo': 'https://www.ingrammicroservices.com/wp-content/uploads/2019/10/logo-asos-black.png',
            'categ':'Fashion'
        })

        self.assertTrue(form.is_valid())

    def test_review_form(self):
         form = ReviewForm(data={
            'title': 'Good',
            'comment': 'verygood',
            'rating': '3.0'
        })

        self.assertTrue(form.is_valid())

    def test_category_form(self):
         form = ReviewForm(data={
            'name': 'Fashion',
            'image': 'http://127.0.0.1:8000/images/cdf2396aab21200f4f6ae8071e46bdde.jpg'
        })

        self.assertTrue(form.is_valid())
    




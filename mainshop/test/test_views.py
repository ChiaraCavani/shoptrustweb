import pprint

from django.test import TestCase, Client
from django.urls import reverse
from mainshop.models import Shop, Review, Category

class TestMainViews(TestCase):

def check(self):
        self.client = Client()
        shop = Shop.objects.create(username='Asos')
        shop.save()
        self.redirect_url = '/?next='
        self.home_url = reverse("mainshop:home")
        self.detail_shop_url = reverse("mainshop:detail")
        self.new_shop_url = reverse("mainshop:newshop")
        self.change_shop_url = reverse("mainshop:change_shop")
        self.delete_shop_url = reverse("mainshop:delete_shop")
        self.review_url = reverse("mainshop:review")
        self.delete_review_url = reverse("mainshop:eliminate")
        self.like_review_url = reverse("mainshop:like_review")
        self.search_url = reverse("mainshop:search_results")
        self.category_url = reverse("mainshop:category")
        self.category_list_url = reverse("mainshop:category_list")
        self.add_category_url = reverse("mainshop:add_category")
        self.delete_category_url = reverse("mainshop:delete_category")


    def test_home(self):
        get_response = self.client.get(self.home_url)
        post_response = self.client.post(self.home_url)

        self.assertEquals(get_response.status_code, 200)
        self.assertEquals(post_response.status_code, 302)

     def test_detail_shop(self):
        get_response = self.client.get(self.detail_shop_url)
        post_response = self.client.post(self.detail_shop_url)

        self.assertEquals(get_response.status_code, 200)
        self.assertEquals(post_response.status_code, 302)

    def test_new_shop(self):
        get_response = self.client.get(self.new_shop_url)
        post_response = self.client.post(self.new_shop_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302)

    def test_change_shop(self):
        get_response = self.client.get(self.change_shop_url)
        post_response = self.client.post(self.change_shop_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_delete_shop(self):
        get_response = self.client.get(self.delete_shop_url)
        post_response = self.client.post(self.delete_shop_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_review(self):
        get_response = self.client.get(self.review_url)
        post_response = self.client.post(self.review_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_delete_review(self):
        get_response = self.client.get(self.delete_review_url)
        post_response = self.client.post(self.delete_review_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 
   
    def test_like_review(self):
        get_response = self.client.get(self.like_review_url)
        post_response = self.client.post(self.like_review_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_search(self):
        get_response = self.client.get(self.search_url)
        post_response = self.client.post(self.search_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

    def test_category(self):
        get_response = self.client.get(self.category_url)
        post_response = self.client.post(self.category_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 
    
    def test_category_list(self):
        get_response = self.client.get(self.category_list_url)
        post_response = self.client.post(self.category_list_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 
    
    def test_add_category(self):
        get_response = self.client.get(self.add_category_url)
        post_response = self.client.post(self.add_category_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 
    
     def test_delete_category(self):
        get_response = self.client.get(self.delete_category_url)
        post_response = self.client.post(self.delete_category_url)

        self.assertEquals(get_response.status_code, 302)
        self.assertEquals(post_response.status_code, 302) 

# Generated by Django 3.1 on 2020-08-27 14:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainshop', '0009_shop_favourite'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shop',
            name='favourite',
        ),
    ]

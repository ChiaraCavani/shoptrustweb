# Generated by Django 3.1 on 2020-08-07 09:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainshop', '0002_shop_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='averageRating',
            field=models.FloatField(default=0),
        ),
    ]

from django import forms
from .models import *

categories = Category.objects.all().values_list('name', 'name')
categories_list = []

for item in categories:
    categories_list.append(item)


# Shop form
class ShopForm(forms.ModelForm):
    class Meta:
        model = Shop
        fields = ['name', 'website', 'logo', 'categ']

        widgets={
            'categ': forms.Select(choices=categories_list, attrs={'class': 'form-control'}),
        }
        
# Review form
class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['title', 'comment', 'rating']


# Category form
class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'image']
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .forms import *
from account.models import Account
from django.contrib.auth.models import User
from django.db.models import Avg, Q
from django.contrib import messages
from django.urls import reverse
from django.views.generic import RedirectView
from http import HTTPStatus
import collections
from django.views.generic import ListView
from datetime import datetime 


""" Main site """
def home(request):
    return render(request, 'mainshop/index.html')

""" This view is the --> Shop detail """
def detail(request, shop_id):
    # Get the shop and the User
    shop = Shop.objects.get(id=shop_id)
    user = request.user.id
    
    # Set the variables
    account= None
    favorited=False
    
    # Default display for reviews
    reviews = Review.objects.filter(shop=shop_id).order_by("-date_pub")
    
    if request.user.is_authenticated:
        # Get the account linked to the User
        account = Account.objects.get(user=user)
        
        # Add shop to account favorite (don't consider the other POST FORM in the template)
        if request.method == 'POST' and "orderform" not in request.POST and "likeform" not in request.POST:
            # If the shop is already in favs remove it after click
            if account.favourite.filter(id=shop_id).exists():
                account.favourite.remove(shop)
            else:
                # If the shop is not in favs add it after click
                account.favourite.add(shop)

        # For button changing
        if account.favourite.filter(id=shop_id).exists():
            favorited = True
        else:
            favorited = False

    # Change order reviews
    if request.method == 'POST' and "orderform" in request.POST:
        choice = request.POST["order"]
        if choice == "Title":
            reviews = Review.objects.filter(shop=shop_id).order_by("title")
            select = "Title"
        elif choice == "Date":
            reviews = Review.objects.filter(shop=shop_id).order_by("-date_pub")
        elif choice == "BestRating":
            reviews = Review.objects.filter(shop=shop_id).order_by("-rating")
            select = "BestRating"
        elif choice == "LowRating":
            reviews = Review.objects.filter(shop=shop_id).order_by("rating")
            select = "LowRating"
     
    # Reviews details
    number_r = reviews.count()
    average = reviews.aggregate(Avg("rating"))["rating__avg"]
           
    if average == None:
        average = 0
    average = round(average, 2)
      
    error = None
    if number_r==0:
        error = "Sadly there's no reviews yet"
            
    context = {
        "shop": shop,
        "reviews": reviews,
        "average": average,
        "total" : number_r,
        "error" : error,
        "favorited": favorited
        
    }
    
    return render(request, 'mainshop/detail.html', context)


""" This view allow the User to give a like to the reviews """
def like_rev(request, shop_id, review_id):
    reviews = Review.objects.get(id=review_id)
    # Check if the user already liked the review
    if reviews.likes.filter(id=request.user.id).exists():
        reviews.likes.remove(request.user)
    else:
        reviews.likes.add(request.user)
    
    return redirect("mainshop:detail", shop_id)

""" Add new shop """
def new_shop(request):
    # Allow to do this only to the superuser (Chiara)
    if request.user.is_superuser:
        if request.method == "POST":
            form = ShopForm(request.POST or None)

            # Check if the form is ok
            if form.is_valid():
                name = form.cleaned_data.get('name')
                
                # Check if the shop already exist
                if Shop.objects.filter(Q(name=name)):
                    error = 'A shop with this name already exist.'
                    return render(request, 'mainshop/newshop.html', {'form': form, 'error': error}, status=HTTPStatus.FOUND)

                # Save data
                data = form.save(commit=False)
                data.save()
                return redirect("mainshop:home")
        else:
            form = ShopForm()
        return render(request, 'mainshop/newshop.html', {"form": form, "controller": "Add new Shop"})
    
    # If the account is not the admin
    else:
        return redirect("mainshop:home")


""" Change information about the shop """
def change_shop(request, id):
    if request.user.is_superuser:
        # Take the id to identify the shop
        shop = Shop.objects.get(id=id)
        if request.method == "POST":
            form = ShopForm(request.POST or None, instance=shop)
            
            if form.is_valid():
                data = form.save(commit=False)
                data.save()
                return redirect("mainshop:detail", id)
        else:
            form = ShopForm(instance=shop)
        return render(request, 'mainshop/changeshop.html', {"form": form})
    
    # If the account is not the admin
    else:
        return redirect("mainshop:home")


""" Delete a shop """
def delete_shop(request, id):
    if request.user.is_superuser:
        #Get the shop by the id
        shop = Shop.objects.get(id=id)

        # Delete
        shop.delete() 
        return redirect ('mainshop:home')

    # If the account is not the admin
    else:
        return redirect("mainshop:home")


""" This view define a category with its shops """
def category(request, cat):
    # Get the category
    category_shop = Shop.objects.filter(categ=cat).order_by('name')
      
    for s in category_shop:
        shop = Shop.objects.get(id=s.id)
        reviews = Review.objects.filter(shop=s.id).order_by("-comment")
        average = reviews.aggregate(Avg("rating"))["rating__avg"]
        
        if average == None:
            average = 0
        
        average = round(average, 2)
        s.averageRating=average
      
        s.save()
    
    return render(request, 'mainshop/category.html', { 'cat':cat, 'shops':category_shop })

""" Display all the categories """
def category_list(request):
    cat_list = Category.objects.all().order_by('name')
    return render(request, 'mainshop/category_list.html', { 'list':cat_list })

""" Insert a new category """
def add_category(request):
    # This can be done only by the superuser
    if request.user.is_superuser:
        if request.method == "POST":
            form = CategoryForm(request.POST or None)

            # Check if the form is ok
            if form.is_valid():
                name = form.cleaned_data.get('name')
                
                # Check if the shop already exist
                if Category.objects.filter(Q(name=name)):
                    error = 'A category with this name already exist.'
                    return render(request, 'mainshop/newshop.html', {'form': form, 'error': error})

                data = form.save(commit=False)
                data.save()
                return redirect("mainshop:home")
        else:
            form = CategoryForm()
        return render(request, 'mainshop/add_category.html', {"form": form, "controller": "Add new category"})
    
    # If the account is not the admin
    else:
        return redirect("mainshop:home")


""" DELETE CATEGORY """
def delete_category(request, cat):
    if request.user.is_superuser:
        #Get the shop by the id
        c = Category.objects.get(name=cat)

        # Delete
        c.delete() 
        return redirect ('mainshop:category_list')

    # If the account is not the admin
    else:
        return redirect("mainshop:home")

""" ADD A REVIEW """
def review(request, id):
    # Only a user can add a review
    if request.user.is_authenticated:
        shop = Shop.objects.get(id=id)

        if request.method == "POST":
            form = ReviewForm(request.POST or None)
            if form.is_valid():
                data = form.save(commit=False)
                data.title = request.POST['title']
                data.comment = request.POST["comment"]
                data.rating = request.POST["rating"]
                data.user = request.user
                data.shop = shop
                data.save()

                return redirect("mainshop:detail", id)
        else:
            form = ReviewForm()
        return render(request, 'mainshop/newreview.html', {"form": form, "shops":shop})
    else:
        return redirect("account:login")


""" Search view """
def searchShop(request):
    # Variables
    count_s=None
    # Query
    query = request.GET.get('q')
    shop = Shop.objects.filter(Q(name__icontains=query))
  
    if shop is not None:
        count_s = shop.count()
    
    context = {
        "query": query,
        "shops": shop,
        "tot" : count_s,
    }

    return render(request, 'mainshop/search_results.html', context)


""" Remove review """
def remove_review(request):
    if request.method == 'POST':
        try:
            review_id = request.POST['review_id']
        except:
            error = "Something wrong with your request."
            return HttpResponseRedirect(reverse('mainshop:home', args=(error,)))
        try:
            review = Review.objects.get(id=review_id)
            print(review.shop.id)
        except:
            return HttpResponseRedirect(reverse('mainshop:detail', args=(review.shop.id,)))
        review.delete()
        return HttpResponseRedirect(reverse('mainshop:detail', args=(review.shop.id,)))
  
    return HttpResponseRedirect(reverse('mainshop:detail'))

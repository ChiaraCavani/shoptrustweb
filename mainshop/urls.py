from django.conf.urls import url
from django.urls import path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = "mainshop"

urlpatterns = [
   url(r'^$', views.home, name='home'),
   url(r'^(?P<shop_id>[0-9]+)/$', views.detail, name='detail'),
   path('newshop/', views.new_shop, name="newshop"),
   url('changeshop/(?P<id>[0-9]+)/', views.change_shop, name="change_shop"),
   url('deleteshop/(?P<id>[0-9]+)/', views.delete_shop, name="delete_shop"),  
   url('review/(?P<id>[0-9]+)/', views.review, name='review'),
   path('remove_review', views.remove_review, name='remove_review'),
   url('like/(?P<shop_id>[0-9]+)/(?P<review_id>[0-9]+)/', views.like_rev, name="likereview"),
   path('search/', views.searchShop, name='search_results'),
   path('category/<str:cat>/', views.category, name='category'),
   path('addcategory/', views.add_category, name='add_category'),
   path('category_list', views.category_list, name='category_list'),
   path('deletecategory/<str:cat>/', views.delete_category, name="delete_category"),  

   
]

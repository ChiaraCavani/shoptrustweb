from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=100)
    image =  models.URLField(default=None, null=True)

    def __str__(self):
        # Return the name of the shop
        return self.name
        

class Shop(models.Model):
    # Fields for the shops table
    name = models.CharField(max_length=100) 
    website = models.CharField(max_length=100)
    categ = models.CharField(max_length=30, default="category", null=True)
    averageRating = models.FloatField(default=0)
    logo = models.URLField(default=None, null=True)
    
    

    def __str__(self):
        # Return the name of the shop
        return self.name


class Review(models.Model):
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.TextField(max_length=50, null=False)
    comment = models.TextField(max_length=5000, null=False)
    
    rating = models.FloatField(default=0, null=False)
    date_pub = models.DateTimeField(auto_now_add=True, verbose_name="date published")
    rev_date =  models.DateField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name="likes_review")
    

    def __str__(self):
        return self.title

    

